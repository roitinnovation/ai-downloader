
## [v1.0.6](https://bitbucket.org/roitinnovation/ai-downloader/compare/v1.0.6..v1.0.5)

2021-06-08

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **downloader:** Adding raised exception FileNotFoundError in delete method.

### Pull Requests

* Merged in hotfix/MGIIA-125 (pull request [#8](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/8/))


## [v1.0.5](https://bitbucket.org/roitinnovation/ai-downloader/compare/v1.0.5..v1.0.4)

2021-05-18

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **downloader:** removed declared NotFound to raise BucketError

### Pull Requests

* Merged in hotfix/AIT-2219 (pull request [#7](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/7/))


## [v1.0.4](https://bitbucket.org/roitinnovation/ai-downloader/compare/v1.0.4..v1.0.3)

2021-05-18

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **downloader:** added retry to NotFound exception on download, upload and delete

### Pull Requests

* Merged in hotfix/AIT-2219 (pull request [#6](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/6/))


## [v1.0.3](https://bitbucket.org/roitinnovation/ai-downloader/compare/v1.0.3..v1.0.2)

2021-05-18

### Chore

* **downloader:** fixing files in modules directory

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **downloader:** fixing storage library version
* **downloader:** fixing call to metadata function
* **downloader:** fixing call to metadata function
* **downloader:** exclude metadata file and adding it in utils file

### Pull Requests

* Merged in hotfix/AIT-2219 (pull request [#5](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/5/))


## [v1.0.2](https://bitbucket.org/roitinnovation/ai-downloader/compare/v1.0.2..v1.0.1)

2021-05-18

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **downloader:** fixing import from modules

### Pull Requests

* Merged in hotfix/AIT-2219 (pull request [#4](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/4/))


## [v1.0.1](https://bitbucket.org/roitinnovation/ai-downloader/compare/v1.0.1..v1.0.0)

2021-05-18

### Docs

* **changelog:** generated CHANGELOG.md file

### Fix

* **downloader:** fixing storage module version to 1.35.0

### Pull Requests

* Merged in hotfix/AIT-2219 (pull request [#3](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/3/))


## v1.0.0

2021-05-18

### Chore

* **downloader:** Removing unecessary files
* **downloader:** Removing readme with wrong informations

### Feat

* **downloader:** Create downloader package to replace downloader micro-service

### Pull Requests

* Merged in develop (pull request [#2](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/2/))
* Merged in feature/AIT-2219 (pull request [#1](https://bitbucket.org/roitinnovation/ai-downloader/pull-requests/1/))

