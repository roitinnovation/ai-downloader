from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name = "Downloader",
    url='https://bitbucket.org/roitinnovation/ai-downloader',
    author = "Victor Cavalhere",
    author_email = "victor.cavalhere@roit.com.br",
    packages=['downloader'],
    install_requires = required,
    version = "1.0.0",
    license='MIT',
    description = ("Secret Manager to ROIT Artificial Intelligence."),
)
