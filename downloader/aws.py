from botocore.exceptions import ParamValidationError, EndpointConnectionError
from boto3.session import Session
from downloader.utils import cronometra
from downloader.errors import *
from base64 import b64decode
import json

class Aws:
    def __init__(self,account_service):

        session = Session(
            aws_access_key_id = account_service['access_key'],
            aws_secret_access_key = account_service['access_secret_key']
        )
        self.__client__ = session.resource('s3')
        
    @cronometra
    def download(self,bucket,blob):
        try:
            obj = self.__client__.Object(bucket, blob)
            data = obj.get()['Body'].read()
        except KeyError as e:
            raise CredentialsError(str(e),"CredentialsError")
        except Exception as e:
            if e.__class__.__name__ == 'NoSuchBucket': raise BucketError(str(e),"BucketErrorDownload")
            elif e.__class__.__name__ == 'NoSuchKey': raise FileNotFoundError(str(e),"FileNameErrorDownload")
            else: raise e
        return data

    @cronometra
    def upload(self,data,bucket,blob):     
        try:
            obj = self.__client__.Object(bucket,blob)
            res = obj.put(Body = b64decode(data))
        except KeyError as e:
            raise CredentialsError(str(e),"CredentialsError")
        except ParamValidationError as e:
            raise UploadError(str(e),"UploadError")
        except EndpointConnectionError as e:
            raise WithoutConnection(str(e), "WithoutConnection")
        except Exception as e:
            if e.__class__.__name__ == 'NoSuchBucket': raise BucketError(str(e),"BucketErrorUpload")
            elif e.__class__.__name__ == 'NoSuchKey': raise FileNotFoundError(str(e),"FileNameErrorUpload")
            else: raise e

        return True if res['ResponseMetadata']['HTTPStatusCode'] == 200 else False
        