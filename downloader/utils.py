import json
import time
from functools import wraps


def cronometra(function):
    @wraps(function)
    def wrapper(*args, **kwrds):
        start = time.time()
        ret = function(*args, **kwrds)
        end = time.time() - start
        print("This is the time that took for", function.__name__, "to finish executing:", end)
        return ret

    return wrapper

def metadata():
    return {
        ".pdf": "application/pdf",
        ".txt": "text/plain",
        ".ofx": "text/plain",
        ".jpg": "image/jpg",
        ".png": "image/jpg",
        ".jpeg": "image/jpg",
        ".bmp": "image/jpg",
        ".gif": "image/jpg",
        ".jpe": "image/jpg",
        ".peg": "image/jpg",
        ".json": "application/json",
        ".gz": "application/gz",
        ".zip": "application/zip",
        ".csv": "application/octet-stream"
    }
