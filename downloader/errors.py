class UploadError(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error


class WithoutConnection(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error


class BucketError(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error


class FileNotFoundError(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error


class CredentialsError(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error


class SecretNotFound(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error


class SecretNotFound(Exception):
    def __init__(self, message, raised_error):
        super().__init__(message)
        self.raised = raised_error
