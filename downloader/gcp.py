from pathlib import Path

from google.cloud import storage
from google.cloud.exceptions import NotFound, ServiceUnavailable
from google.oauth2 import service_account
from retry import retry

from downloader.errors import *
from downloader.utils import cronometra, metadata


class Gcp:
    def __init__(self, account_service):

        credentials = service_account.Credentials.from_service_account_info(account_service)
        self.__client__ = storage.Client(credentials=credentials, project=account_service['project_id'])
        self.__metadata__ = metadata()

    @cronometra
    @retry((ServiceUnavailable, NotFound), tries=3, delay=2)
    def download(self, bucket, blob):
        try:
            bucket = self.__client__.get_bucket(bucket)
            arq = bucket.get_blob(blob)
            data = arq.download_as_string()
        except AttributeError as e:
            raise FileNotFoundError(str(e), "FileNameErrorDownload")
        return data

    @cronometra
    @retry((ServiceUnavailable, NotFound), tries=3, delay=2)
    def upload(self, data, bucket, blob):
        try:
            bucket = self.__client__.get_bucket(bucket)
            up_blob = bucket.blob(blob)
            up_blob.upload_from_string(data, content_type=self.__metadata__[Path(blob).suffix.lower()])
        except TypeError as e:
            raise UploadError(str(e), "UploadError")
        return True

    @cronometra
    @retry((ServiceUnavailable, NotFound), tries=3, delay=2)
    def delete(self, bucket, blob):
        try:
            bucket = self.__client__.get_bucket(bucket)
            blob = bucket.get_blob(blob)
            blob.delete()
            return True
        except AttributeError as e:
            raise FileNotFoundError(str(e), "FileNameErrorDownload")
